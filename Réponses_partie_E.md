| Failles                    | Corrections mises en place |
|----------------------------|----------------------------|
| Injection SQL              | Utilisation des requêtes préparées voir **requêtes préparées**|
| Cross-site scripting (XSS) | Utilisation de la fonction php ```htmlspecialchars``` : Ajout de cette ligne dans la fonction addMessage :  ```$body = htmlspecialchars($body);```|
| Violation de contrôle d’accès|Empêcher les personnes d'accéder aux messages d'une autre personne : ```if($userid != $_SESSION["ID_USER"]) {$url_redirect = "vw_moncompte.php";}```|
| Violation de gestion de session | Utilisation du .htaccess : ajouter la ligne suivante : ```php_value session.cookie_httponly 1```. Le cookie n'est donc accessible plus que via le protocole HTTP   |
| Falsification de requête (CSRF) |  Utilisation de la fonction php ```htmlspecialchars``` : Ajout de cette ligne dans la fonction addMessage :  ```$body = htmlspecialchars($body);``` |
| Vulnérabilité d'un composant |  Changer les mots de passe par défaut (MySQL , PHPMyAdmin etc...)  |
| Chiffrement des données sensibles |  Ajouter cette ligne dans le .htaccess pour cacher le fichier config : ``` RedirectMatch 404 \.(htaccess\|ini)$ ``` <br /><br /> Il faudrait ajouter les mots de passes hachés plutôt que les mots de passes en clair (avec l'algorithme BCRYPT par exemple). Ainsi, avant la requête de login/password, on ajoute la ligne ```$passwordHashed = password_hash($password, PASSWORD_BCRYPT);```|
| Accès aux répertoires par http |  Ajouter cette ligne dans le .htaccess : ```RedirectMatch 403 ^/sr03p20/config/.*$```  |
| Scripts de redirection |  Plutôt que de laisser une URL dynamique comme cela : ```$url_redirect = $_REQUEST['loginPage'] ;```, on devrait entrer cette URL en dur : ```$url_redirect = '/sr03p20/';```  |

**requêtes préparées**

L'exécution de la requête de recherche de login/password devrait se faire de la manière suivante : 
```
$req= $mysqli->prepare("select NAME, FIRSTNAME, LOGIN, ID_USER, ACCOUNT_NUMBER, USER_PROFIL, ACCOUNT_SOLD from users where LOGIN= ? and PASSWORD= ?");
      $req->bind_param('ss', $login, $pwd);
      if (! $req->execute()) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
          $utilisateur = false;
      } else {
          $result = $req->get_result();
          $row = $result->fetch_assoc();
          if (empty($row)) {
            $utilisateur = false;
          } else {
            $utilisateur= $row;
          }
```
          